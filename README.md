### A Tutorial on First-Class Pattern Matching

#### Introduction

Algebraic data types is an essential instrument at designing functional program in statically types languages like OCaml. They support an effective compilation scheme and allow exhaustiveness check for code with pattern matching. These check are very helpful when one needs to write the code when performs different actions in different cases.

But the essence of ADTs has a few shortcomings. One of them is that an ability to pattern match on the algebraic data types contradicts the concept of abstract data types. Indeed, we should have constructors in scope to be able to effectively pattern-match on them. Because of that ADT's constructors and its arguments leak to public interfaces of modules, and end-users of our modules are obliged to adapt their code when new constructors or arguments are added.

In this tutorial we explain a technique to pattern match on the contents of data types without using a pattern matching language feature of OCaml. So called pattern matching combinators is a useful approach when one needs to check for a single case from a myriad of possible algebraic values, and perform a standard action for all other forms (for example, do nothing). This approach allows us to have less coupling between our modules, but to do that we are going to sacrifice exhaustiveness checks and some performance. The patterns are first-class values, we are free to pass too and return them from functions, traditional pattern matching doesn't have this ability


#### The example language

In the next section we are going to present an implementation similar to the one from PPXlib, and test it on the language of Untyped Lambda Calculus (ULC). The language has only three constructions:

````ocaml
module Lam : sig
  type lam =
    | Var of string  (** Variables [x] *)
    | App of lam * lam  (** Application: [f g] *)
    | Abs of string * lam  (** Abstraction: [fun x -> b] *)
  ...
end = struct
  ...
end
````

In this language can express any possible computation, because untyped lambda calculus is Turing complete. For example,
`Abs ("s", Var "s")` and an identity function `Stdlib.Fun.id`. The `Abs ("x", Abs ("y", Var "x"))` in OCaml syntax is
`fun x y -> x` and called `Stdlib.Fun.const`. It takes two values and returns the first one, in the language of ULC it is used as `true` or `fst`. The lambda-expression `Abs ("y", Abs ("x", Var "x"))` does the opposite and in the ULC world is called `false` or `snd`. We are going to use these citizens of type `Lam.lam` as examples.

All occurrences of variables in lambda expression may be classified as "free" or "not free". The occurrence of variable "x" is called "not free" iff it is located in the body of some lambda abstraction which introduces the variable "x". A couple of examples:

* In `Abs ("x", Var "x")` single occurence of `Var "x"` is not free because it is inside lambda-abstraction `Abs ("x", Var "x")`
* In `Abs ("y", App (Var "x", Abs ("x", Var "x")))` the right occurence of `Var "x"` is not free by the same reason as above.
  The left occurrence of `Var "x"` is free because there is not lambda-abstraction on `"x"` that contains this occurence. The only occurrence is `Abs ("y", ...)` but it is for another variable (`"y"`).

Using this definition we could write a function `is_free` that checks if there is occurence of variable `name` in the expression `inside`:

````ocaml
let rec is_free name inside =
  match inside with
  | Var s when s = name -> false
  | Var _ -> true
  | App (l, r) -> is_free name l || is_free name r
  | Abs (s, _) when s = name -> true
  | Abs (_, b) -> is_free name b
````

We are going to use the function `is_free` in examples below.


#### Simple patterns

Let's add some basic definitions of first-class patterns and put them into module `Ast_pattern0`.

````ocaml
module Ast_pattern0 = struct
  type loc = unit

  exception Expected of loc * string

  let fail loc expected = raise (Expected (loc, expected))

  type context = ...

  type ('matched_value, 'k, 'k_result) t =
    | T of (context -> loc -> 'matched_value -> 'k -> 'k_result)
end
````

In the PPXlib implementation patterns also report location within error messages. In our simple case we are going to ignore location and use simple type `type loc = unit`. In the circumstances when the pattern doesn't fit, we are going to report error using the function `Ast_pattern0.fail`.

The type of first class patterns has three parameters:
  * the value that is being matched
  * the continuation function `'k` which may take subvalues of original value and return a value of type `'k_result`
  * the return `'k_result` of this pattern matching

Module `Ast_pattern0` also defines `context` type which will be discussed in the section about parsing with alternatives.

Now we are going to present a most useful pattern combinator called `__`. Despite it is starting from underscore, it corresponds to pattern variables in traditional OCaml, not wildcards.

````ocaml
let __ : 'a 'b. ('a, 'a -> 'b, 'b) t =
  T (fun ctx _loc x k -> k x)
````

Every first class pattern is a function which accepts matchable value (a.k.a. scrutinee) `x` and continuation `k`. In this implementation we pass scrutinee to the continuation as-is, this value will be examined later by user-provided code (which in traditional pattern matching is located on the right hand side of the matching clause, just after the `->` symbol). The simple threading of scrutinee is reflected in the type of the first class pattern: we match on values of type `'a`, continuation will take this value `'a` and will return some result of type `'b`, and the return value of type `'b`. This pattern is polymorphic and doesn't involve our type for lambda expression. It's going  to be useful for any values of any type we are going to pattern-match.

Let's define a specific pattern combinator for lambda-abstraction in our language of untyped lambda calculus. The constructor `Abs` has two arguments, so our pattern combinator will accept two pattern arguments too. But in principle if we do not want to allow users to match on every subvalue, we would have less arguments.

````ocaml
let abs : 'a 'b 'c. (string, 'a, 'b) t -> (lam, 'b, 'c) t -> (lam, 'a, 'c) t =
  fun (T fleft) (T fright) ->
    T
      (fun ctx loc x k ->
        match x with
        | Abs (left, right) ->
            k |> fleft ctx loc left |> fright ctx loc right
        | _ -> fail loc "abstraction")
````

If the provided scrutinee is not an abstraction, we report failure. Otherwise, we thread continuation through the left and right first class pattern. First pattern takes our continuation of type `'a` and creates the result of type `'b`. Second pattern converts continuation of type `'b` into `'c`, so pattern combinator `abs` gives the resulting pattern which converts continuation of type `'a` into `'c`.

Now we can combine pattern `__` and pattern pattern combinator `abs` and get, for example:

````ocaml
let (_ : (lam, string -> lam -> 'a, 'a) Ast_pattern0.t) =
  let open Patterns in
  let open Ast_pattern in
  abs __ __
````

In the example above we used pattern `__` twice: once in the place we expected a pattern to match a value of type `string` and in the second place to match for `lam`. If pattern matching succeeds, the user code will be a continuation which takes two arguments: name of variable under abstract and body of the abstraction.

Let's test our pattern on an example. We are going to use a special function `parse` to run pattern matching. It takes
  * A first class pattern `('a, 'b, 'c) t` as usual pattern matching does
  * Optional `on_error:(string -> 'c)` handler. In standard pattern matching it corresponds to the exception `Match_failure`.
  * A scrutinee of type `'a`
  * A user-provided handler of  type `'b` for subvalues extracted by matching.
  * And returns results of type `'c`.

````ocaml
let parse :
      'a 'b 'c.
      ('a, 'b, 'c) t -> loc -> ?on_error:(string -> 'c) -> 'a -> 'b -> 'c =
  fun (T f) loc ?on_error x k ->
  try f { matched = 0 } loc x k
  with Expected (_loc, expected) -> (
    match on_error with
    | None -> failwith (Printf.sprintf "%s expected" expected)
    | Some f -> f expected)
````

If pattern matching fails, it may run a user-provided error handler when some default result will be returned. In principle, we could write in that handler another invocation of `parse` with another pattern, but it's recommended to use an alternation combinator for that (see below).

Now we can write a test with our pattern `abs __ __`. We are going to test that the provided lambda expression is indeed an abstraction. The success continuation will take two subvalues of abstraction, ignore them and return true. The `on_error`
(continuation) reports via `false` that the test has failed.

````ocaml
let is_an_abstraction e =
  let pat =
    let open Patterns in
    let open Ast_pattern in
    abs __ __
  in
  Ast_pattern.parse pat ()
    ~on_error:(fun _msg -> false)
    e
    (fun _ _ -> true)

let%test _ = is_an_abstraction (Abs ("x", Var "x"))
let%test _ = not (is_an_abstraction (Var "x"))
````

##### Exercise 1. Drop

In the test above we matched two subvalues of abstraction, but we actually were not interested in them. Define pattern combinator `drop` which

* accepts any value (similar to `__`)
* doesn't force user to accept this values in the success continuation (not like `__`)

Test your implementation on the following snippet:

````ocaml
let is_an_abstraction e =
  let pat =
    let open Patterns in
    let open Ast_pattern in
    abs drop drop
  in
  Ast_pattern.parse pat ()
    ~on_error:(fun _msg -> false)
    e
    true

let%test _ = is_an_abstraction (Abs ("x", Var "x"))
````

##### Modifying results of the matching

In some cases we need to modify or perform some additional checks on subvalues of the scrutinee.

Let's imagine that we want to delay some evaluations in ULC. In OCaml we would write `(fun () -> ...)`, and apply it to `()` when we will need to force the computatiton. But in ULC we don't have values of type `unit`, so we need to use an abstraction with a variable name, and apply it to some lambda expression when we will need to force a value.
But the name in the abstraction couldn't be arbitrary: there is a chance that the new name we connect to one of free variables inside expression being delayed, which could drastically change semantics of expression in ULC. For example,

* We want to delay evaluation of `App (Var "f", Var "x")`. We could delay it by introducing an new name `"zzz"` getting
  `Abs ("zzz", App (Var "f", Var "x") )` which is completely fine. Applying this lambda expression to anything will give us back
  `App (Var "f", Var "x")`.
* But we could use a wrong name (for example, `"x"`) instead of `"zzz"`. Now the application of
  `Abs ("x", App (Var "f", Var "x") )` to `something` will give use `App (Var "f", something)` which is completely different from the original `App (Var "f", Var "x")`. Delaying the computation has been done wrong.

And now we are going to define a first class pattern for matching for properly delayed values. Let's look at straightforward implementation:

````ocaml
let delay : 'a . (Lam.lam, 'a, 'a) Ast_pattern0.t =
  let open Ast_pattern0 in
  T
    (fun ctx loc x k ->
      match x with
      | Lam.Abs (name, b) when Lam.is_free name b ->
          k
      | _ -> fail loc "delay")
````

This definition, in principle, is correct, but it neglect the original idea of first-class pattern matching. If the type of lambda expression is abstract (and constructors are not visible), we couldn't typecheck this definition of the pattern. It's better to use the pattern `abs __ __` and "map" the result of the matching.

We are going to use a function `map2` which fits for two-argument continuations. It  "maps" the result in the same manner as `List.map` applies a function for all elements of the list.

````ocaml
let map2 :
      'a 'b 'c 'd 'e.
      ('a, 'b -> 'c -> 'd, 'e) t -> f:('b -> 'c -> 'f) -> ('a, 'f -> 'd, 'e) t
    =
  fun (T func) ~f ->
  T (fun ctx loc x k -> func ctx loc x (fun a b -> k (f a b)))
````

With that function we could rewrite pattern `delay` without mentioning constructor's names:

````ocaml
let delay2 () =
  let open Ast_pattern0 in
  let open Ast_pattern in
  let open Lam.Patterns in
  abs __ __
  |> map2 ~f:(fun name body -> if Lam.is_free name body then () else fail () "not a proper delay")

let%test _ =
  Ast_pattern.parse (delay2 ()) ()
    ~on_error:(fun _msg -> false)
    (Abs ("x", Var "y"))
    (fun () -> true)
````

N.B. In the definition above we added an extra `()` argument to delay evaluation of the pattern `delay2`. In this concrete case it is not mandatory, but omitting it will lead to less polymorphic types. For more details study the concept of *relaxed value restriction*.

##### Exercise 2. More mapping functions

The library and PPXlib defines a small number of "map"-like functions: `map0` ... `map5`. Find a way to construct `mapN` where N>5 from these 6 functions.

##### Exercise 3. Polyvariadic mapping

*Research question*. In principle, it should be possible to have a single mapping function, which will have a right arity after applying to a so-called *numeral*. [Do some "Olegging"](https://okmij.org/ftp/Computation/extra-polymorphism.html#poly-var) and define this function.


#### Alternation (TODO)

I do not entirely understand why matching counter in the context is mandatory. Maybe somebody else should write this section.


#### Related work

The first definitions of concepts "patterns" and "pattern matching" that are not tied to the concrete programming langauge
were introduced by M. Tullsen (in the paper "First Class Patterns", 2000). There he proposed to define a pattern as a function  which accepts scrutinees of type `'a` and may extract values of type `'b` from there. He also declared pattern combinators which are truely first class, i.e. could be passed to and be returned from functions.


````ocaml
type ('a, 'b) pat = 'a -> 'b option

(∗ or ∗)
val ($|): ('a, 'b) pat -> ('a, 'b) pat -> ('a, 'b) pat

(∗ and ∗)
val ($&): ('a, 'b) pat -> ('a, 'b) pat -> ('a, 'b ∗ 'b) pat

(∗ composition ∗)
val ($:): ('a, 'b) pat -> ('b, 'c) pat -> ('a, 'c) pat

(∗ Functor mapping ∗)
val ($>): ('a, 'b) pat -> ('b -> 'c) -> ('a, 'c) pat

(∗ parallel matching ∗)
val ( $∗ ): ('a, 'c) pat -> ('b, 'd) pat -> ('a ∗ 'c, 'b ∗ 'd) pat
````


##### Active patterns in F#

The described approach is language-agnostic which is great, but implementations with a compiler support should have better performace characteristics. One of these approaches is "Active patterns" from F# language. It introduces special type of pattern values and special syntax to define them.

````fsharp
let (|Delay|_|) e =
  match e with
  | Abs(v, body) when Lam.is_free v body -> Some body
  | _ -> None
````

In the point of usage active patterns do not syntactically differ from normal constructors. The resulting code may perplex reader to think that normal patterns are used and compilation scheme is efficient (no).

````fsharp
let is_delay = function
  | Delay _ -> true
  | _ -> false
````

##### ViewPatterns in Haskell

Let's look at abstract implementation of abstract list, which have two smart constructors and single function `head` for destructing a list

````Haskell
class AbsList c where
  nil  :: c a
  cons :: a -> c a -> c a
  head :: c a -> Maybe (a, c a)
````

For this interface we may want to write a `filterMap` function, which applies function `f` to every element of the list and collects to the resulting list answers tagged by `Just` constructor.

We could use transformational patterns (a paper "Pattern Guards and Transformational Patterns" 2000) to define this function. In the snippet above the syntax `pat <- right` checks that the value `right` matches with the pattern `left` and pattern variables inside `left` are being introduced into the scope.

````Haskell
filterMap :: AbsList c => c a -> (a -> Maybe b) -> c b
filterMap f l
  | Just (x, xs) <- head l,
    Just v <- f x            = v `cons` filterMap f xs
  | Just (x, xs) <- head l,
    Nothing <- f x           = filterMap f xs
  | Nothing <- head l        = nil
````

In the snippet above has a few duplications inside it, for example `Just (x, xs) <- head l`.  Haskell's *view patterns* could overcome this annoyance:

````Haskell
{−# LANGUAGE ViewPatterns #−}
filterMap f (head -> Just (f -> Just v, xs))
  = v `cons` filterMap f xs
filterMap f (head -> Just (f -> Nothing, xs))
  = filterMap f xs
filterMap f (head -> Nothing)
  = nil
````

##### Scala 2 Object Patterns

In Scala 2 the one could define special objects called "extractors" with a single method "unapply", which strongly remind us  active patterns

````scala
trait AbsList[+A] {
  def nil[B]: AbsList[B]
  def cons[B, U >: B](hd: U, tl: AbsList[B]): AbsList[U]
  def head: Option[(A, AbsList[A])]
  // Extractor signatures:
  trait ConsExSig {
    def unapply[A](l: AbsList[A]): Option[(A, AbsList[A])] =
      l.head
  }
  trait NilExSig {
    def unapply[A](l: AbsList[A]): Boolean = l.head.isEmpty
  }
  // Extractors
  val Cons = new ConsExSig {}
  val Nil = new NilExSig {}
}
````

````scala
def filterMap[A, B](f: A ⇒ Option[B], l: AbsList[A])
    : AbsList[B] = {
  import l._
  l match {
    case Cons(x, xs) if f(x).isDefined ⇒
      cons(f(x).get, filterMap(f, xs))
    case Cons(_, xs) ⇒
      filterMap(f, xs)
    case Nil() ⇒
      nil
  }
}
````

#### Acknowledgements

#### TODO

* alternation
* Reread related works (maybe we need to provide examples about lambda calculus, and not about lists. Or abandon lambda calculus and use simple lists)
* acknowledgements
