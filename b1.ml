open Benchmark
open Lib1

let repeat = 5
let timeout = 1

module ManyConstrs = struct
  let name = ""

  let create n =
    let rec loop acc n =
      if n <= 0 then acc else loop (Lam.Abs ("x", acc)) (n - 1)
    in
    loop (Var "x") n

  module A = struct
    let fold = function
      | Lam.Abs
          ( _,
            Abs
              ( _,
                Abs
                  ( _,
                    Abs
                      ( _,
                        Abs (_, Abs (_, Abs (_, Abs (_, Abs (_, Abs (_, _))))))
                      ) ) ) ) ->
          true
      | _ -> false
  end

  module B = struct
    open Ast_pattern

    let pat =
      let open Lam.Patterns in
      abs drop
        (abs drop
           (abs drop
              (abs drop
                 (abs drop
                    (abs drop (abs drop (abs drop (abs drop (abs drop __)))))))))

    let fold lam =
      Ast_pattern.parse pat ()
        ~on_error:(fun _msg -> false)
        lam
        (fun _prev -> true)
  end
end

let () =
  let module M = ManyConstrs in
  let xs = M.create 20 in
  let wrap f () =
    let _ = f xs in
    ()
  in
  let res =
    throughputN ~repeat timeout
      [
        (Printf.sprintf "%s_D_" M.name, wrap M.A.fold, ());
        (Printf.sprintf "%s_V_" M.name, wrap M.B.fold, ());
      ]
  in
  print_newline ();
  tabulate res

let find_static_exn m =
  let exception Return of (int * int) in
  let r = ref 0. in
  try
    for i = 0 to Array.length m - 1 do
      let a = m.(i) in
      for j = 0 to Array.length a - 1 do
        r := !r +. a.(j);
        if !r < 0. then raise (Return (i, j))
      done
    done;
    (-1, -1)
  with Return x -> x
