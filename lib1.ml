module Ast_pattern0 = struct
  type loc = unit

  exception Expected of loc * string

  let fail loc expected = raise (Expected (loc, expected))

  type context = {
    (* [matched] counts how many constructors have been matched. This is used to find what
       pattern matches the most some piece of lam in [lam_pattern.alt]. In the case where
       all branches fail to match, we report the error from the one that matches the
       most.
       This is only incremented by combinators that can fail. *)
    mutable matched : int;
  }

  type ('matched_value, 'k, 'k_result) t =
    | T of (context -> loc -> 'matched_value -> 'k -> 'k_result)

  (* end of copy-plame from https://github.com/ocaml-ppx/ppxlib/blob/0.22.2/src/lam_pattern0.ml *)
  (* TODO: deal with licencing issues *)
end

module Ast_pattern = struct
  open Ast_pattern0

  let save_context ctx = ctx.matched
  let restore_context ctx backup = ctx.matched <- backup
  let incr_matched c = c.matched <- c.matched + 1

  let parse :
        'a 'b 'c.
        ('a, 'b, 'c) t -> loc -> ?on_error:(string -> 'c) -> 'a -> 'b -> 'c =
   fun (T f) loc ?on_error x k ->
    try f { matched = 0 } loc x k
    with Expected (_loc, expected) -> (
      match on_error with
      | None -> failwith (Printf.sprintf "%s expected" expected)
      | Some f -> f expected)

  let __ : 'a 'b. ('a, 'a -> 'b, 'b) t =
    T
      (fun ctx _loc x k ->
        incr_matched ctx;
        k x)

  let drop : 'a 'b. ('a, 'b, 'b) t =
    T
      (fun ctx _loc _ k ->
        incr_matched ctx;
        k)

  let cst ~to_string ?(equal = ( = )) v =
    T
      (fun ctx loc x k ->
        if equal x v then (
          incr_matched ctx;
          k)
        else fail loc (to_string v))

  let string v = cst ~to_string:(Printf.sprintf "%S") v

  let alt (T f1) (T f2) =
    T
      (fun ctx loc x k ->
        let backup = save_context ctx in
        try f1 ctx loc x k
        with e1 -> (
          let m1 = save_context ctx in
          restore_context ctx backup;
          try f2 ctx loc x k
          with e2 ->
            let m2 = save_context ctx in
            if m1 >= m2 then (
              restore_context ctx m1;
              raise e1)
            else raise e2))

  let ( ||| ) = alt
  let map (T func) ~f = T (fun ctx loc x k -> func ctx loc x (f k))
  let ( >>| ) t f = map t ~f
  let map0 (T func) ~f = T (fun ctx loc x k -> func ctx loc x (k f))

  let map1 (T func) ~f =
    T (fun ctx loc x k -> func ctx loc x (fun a -> k (f a)))

  let map2 :
        'a 'b 'c 'd 'e.
        ('a, 'b -> 'c -> 'd, 'e) t -> f:('b -> 'c -> 'f) -> ('a, 'f -> 'd, 'e) t
      =
   fun (T func) ~f ->
    T (fun ctx loc x k -> func ctx loc x (fun a b -> k (f a b)))

  let map3 (T func) ~f =
    T (fun ctx loc x k -> func ctx loc x (fun a b c -> k (f a b c)))

  let of_func f = T f
  let to_func (T f) = f

  (* TODO: tests *)
  (* Choice: longer match is a result  *)
  let choice_lmf = function
    | [] -> failwith "Bad argument"
    | h :: tl -> List.fold_left alt h tl

  (* Alternative like in combinators. First success is an answer *)
  let choice : 'a 'b 'c. ('a, 'b, 'c) t list -> ('a, 'b, 'c) t =
   fun ps ->
    T
      (fun ctx loc x k ->
        let rec helper = function
          | [] -> fail () "Choice failed"
          | h :: tl -> (
              try to_func h ctx loc x k with Expected _ -> helper tl)
        in

        helper ps)

  (* TODO: pack *)
end

module Lam : sig
  type lam =
    | Var of string  (** Variables [x] *)
    | App of lam * lam  (** Application: [f g] *)
    | Abs of string * lam  (** Abstraction: [fun x -> b] *)

  val is_free : string -> lam -> bool

  module Patterns : sig
    open Ast_pattern0

    val var : (string, 'a, 'b) t -> (lam, 'a, 'b) t
    val app : (lam, 'a, 'b) t -> (lam, 'b, 'c) t -> (lam, 'a, 'c) t
    val abs : (string, 'a, 'b) t -> (lam, 'b, 'c) t -> (lam, 'a, 'c) t
  end
end = struct
  type lam = Var of string | App of lam * lam | Abs of string * lam

  let true_ = Abs ("x", Abs ("y", Var "x"))

  let s =
    Abs
      ( "f",
        Abs
          ("g", Abs ("x", App (App (Var "f", Var "x"), App (Var "g", Var "x"))))
      )

  let rec is_free name inside =
    match inside with
    | Var s when s = name -> false
    | Var _ -> true
    | App (l, r) -> is_free name l || is_free name r
    | Abs (s, _) when s = name -> true
    | Abs (_, b) -> is_free name b

  module Patterns = struct
    open Ast_pattern0

    let var : 'a 'b. (string, 'a, 'b) t -> (lam, 'a, 'b) t =
     fun (T fname) ->
      T
        (fun ctx loc x k ->
          match x with
          | Var name ->
              ctx.matched <- ctx.matched + 1;
              fname ctx loc name k
          | _ -> fail loc "variable")

    let app (T fleft) (T fright) =
      T
        (fun ctx loc x k ->
          match x with
          | App (left, right) ->
              ctx.matched <- ctx.matched + 1;
              k |> fleft ctx loc left |> fright ctx loc right
          | _ -> fail loc "application")

    let abs : 'a 'b 'c. (string, 'a, 'b) t -> (lam, 'b, 'c) t -> (lam, 'a, 'c) t
        =
     fun (T fleft) (T fright) ->
      T
        (fun ctx loc x k ->
          match x with
          | Abs (left, right) ->
              ctx.matched <- ctx.matched + 1;
              k |> fleft ctx loc left |> fright ctx loc right
          | _ -> fail loc "abstraction")
  end

  let (_ : (lam, string -> lam -> 'a, 'a) Ast_pattern0.t) =
    let open Patterns in
    let open Ast_pattern in
    abs __ __

  let%test _ =
    let pat =
      let open Patterns in
      let open Ast_pattern in
      abs __ (var __)
    in
    Ast_pattern.parse pat ()
      ~on_error:(fun _msg -> false)
      (Abs ("x", Var "x"))
      (fun varleft varright -> varleft = varright)

  let identity =
    let open Ast_pattern0 in
    T
      (fun ctx loc x k ->
        match x with
        | Abs (x, Var y) when x = y ->
            ctx.matched <- ctx.matched + 1;
            k
        | _ -> fail loc "abstraction")

  let%test _ =
    Ast_pattern.parse identity ()
      ~on_error:(fun _msg -> false)
      (Abs ("x", Var "x"))
      true
end

let delay : 'a. (Lam.lam, 'a, 'a) Ast_pattern0.t =
  let open Ast_pattern0 in
  T
    (fun ctx loc x k ->
      match x with
      | Lam.Abs (name, b) when Lam.is_free name b ->
          ctx.matched <- ctx.matched + 1;
          k
      | _ -> fail loc "delay")

let%test _ =
  Ast_pattern.parse delay ()
    ~on_error:(fun _msg -> false)
    (Abs ("x", Var "y"))
    true

let delay2 () =
  let open Ast_pattern0 in
  let open Ast_pattern in
  let open Lam.Patterns in
  abs __ __
  |> map2 ~f:(fun name body -> if Lam.is_free name body then () else fail () "")

let%test _ =
  Ast_pattern.parse (delay2 ()) ()
    ~on_error:(fun _msg -> false)
    (Abs ("x", Var "y"))
    (fun () -> true)

let describe () =
  let open Ast_pattern in
  let open Ast_pattern0 in
  let open Lam.Patterns in
  map1 __ ~f:(fun _ -> fail () "Just an example")
  ||| (abs __ (abs __ (var __))
      |> map3 ~f:(fun a b c -> if a = c && b <> a then `True else fail () ""))
  ||| (abs __ (abs __ (var __))
      |> map3 ~f:(fun a b c -> if b = c && b <> a then `False else fail () ""))
  ||| (__ |> map1 ~f:(fun x -> `Other x))

let%test _ =
  Ast_pattern.parse (describe ()) ()
    ~on_error:(fun _msg -> false)
    (Abs ("x", Var "y"))
    (function `Other _ -> true | _ -> false)

let%test _ =
  Ast_pattern.parse (describe ()) ()
    ~on_error:(fun _msg -> false)
    (Abs ("u", Abs ("v", Var "v")))
    (function `False -> true | _ -> false)

(*
module LamGADT = struct
  type (_, _) lam =
    | Var : ('gamma * 't, 't) lam
    | App : ('gamma, 't1 -> 't2) lam * ('gamma, 't1) lam -> ('gamma, 't2) lam
    | Abs : ('gamma * 't1, 't2) lam -> ('gamma, 't1 -> 't2) lam

  module Patterns = struct
    open Ast_pattern0

    (* let app : _ =
       fun (T fleft) (T fright) ->
        T
          (fun ctx loc x k ->
            match x with
            | App (left, right) ->
                ctx.matched <- ctx.matched + 1;
                k |> fleft ctx loc left |> fright ctx loc right
            | _ -> fail loc "application") *)

    let app : _ =
     fun (T fleft) (T fright) ->
      let rec f (type a b) ctx loc (x : (a, b) lam) k =
        match x with
        | App (left, right) ->
            ctx.matched <- ctx.matched + 1;
            k |> fleft ctx loc left |> fright ctx loc right
        | _ -> fail loc "application"
      in
      Ast_pattern.of_func f
  end
end
*)
module PeanoGADT = struct
  type (_, _) var =
    | Z : ('a, 'a * 'g) var
    | S : ('a, 'g) var -> ('a, 'b * 'g) var

  let _1 = S Z

  module Patterns = struct
    open Ast_pattern0

    let s : ((_, _) var, 'a, 'b) t -> ((_, _) var, 'a, 'b) t =
     fun (T fprev) ->
      T
        (fun ctx loc x k ->
          match x with
          | S p ->
              ctx.matched <- ctx.matched + 1;
              k |> fprev ctx loc p
          | Z -> fail loc "z")

    let z : ((_, _) var, 'a, 'b) t =
      T
        (fun ctx loc x k ->
          match x with
          | Z ->
              ctx.matched <- ctx.matched + 1;
              k
          | _ -> fail loc "variable")
  end

  let%test _ =
    let pat_ge_three =
      let open Patterns in
      let open Ast_pattern in
      s (s (s __))
    in
    Ast_pattern.parse pat_ge_three ()
      ~on_error:(fun _msg -> false)
      (S (S (S Z)))
      (fun _prev -> true)
end

module _ = struct
  open Typedtree
  open Ast_pattern0

  let pat_lazy (T f) =
    let rec helper (type a) ctx loc (x : a general_pattern) k =
      match x.pat_desc with
      | Tpat_lazy y -> f ctx loc y k
      | Tpat_value v -> helper ctx loc (v :> Typedtree.pattern) k
      | _ -> fail () "value pattern"
    in
    T helper
end

let _ = Fun.const
